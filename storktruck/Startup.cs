﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(storktruck.Startup))]
namespace storktruck
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
