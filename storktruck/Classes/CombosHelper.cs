﻿using storktruck.Models;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections;

namespace storktruck.Classes
{
    public class CombosHelper
    {
        private static DataContext db = new DataContext();

        public static List<State> GetStates()
        {
            var states = db.States.ToList();

            states.Add(new State
            {
                StateId = 0,
                Name = "[Selecione um Registro ...]"
            });

            return states = states.OrderBy(d => d.Name).ToList();

        }

        public static List<State> GetStates(int countryId)
        {
            var states = db.States.Where(e => e.CountryId == countryId).ToList();

            states.Add(new State
            {
                StateId = 0,
                Name = "[Selecione um Registro ...]"
            });

            return states = states.OrderBy(d => d.Name).ToList();
        }

        public static List<Partner> GetPartners()
        {
            var partners = db.Partners.ToList();

            partners.Add(new Partner
            {
                PartnerId = 0,
                Name = "[Selecione um Parceiro ...]"
            });

            return partners = partners.OrderBy(c => c.Name).ToList();
        }

        public static List<Model> GetModels()
        {
            var models = db.Models.ToList();

            models.Add(new Model
            {
                ModelId = 0,
                ModelName = "[Selecione um Modelo ...]"
            });

            return models = models.OrderBy(c => c.ModelName).ToList();
        }

        public static List<Model> GetModels(int modelId)
        {
            var models = db.Models.Where(b => b.ModelId == modelId).ToList();

            models.Add(new Model
            {
                ModelId = 0,
                ModelName = "[Selecione um Modelo ...]"
            });

            return models = models.OrderBy(c => c.ModelName).ToList();
        }

        public static List<Brand> GetBrands()
        {
            var brands = db.Brands.ToList();

            brands.Add(new Brand
            {
                BrandId = 0,
                BrandName = "[Selecione uma Marca ...]"
            });

            return brands = brands.OrderBy(c => c.BrandName).ToList();
        }

        public static List<Brand> GetBrands(int brandId)
        {
            var brands = db.Brands.Where(b => b.BrandId == brandId).ToList();

            brands.Add(new Brand
            {
                BrandId = 0,
                BrandName = "[Selecione uma Marca ...]"
            });

            return brands = brands.OrderBy(c => c.BrandName).ToList();
        }

        public static List<Partner> GetPartners(int partnerId)
        {
            var partners = db.Partners.Where(a => a.PartnerId == partnerId).ToList();

            partners.Add(new Partner
            {
                PartnerId = 0,
                Name = "[Selecione um Parceiro ...]"
            });

            return partners = partners.OrderBy(c => c.Name).ToList();
        }

        public static List<City> GetCities()
        {
            var cities = db.Cities.ToList();

            cities.Add(new City
            {
                CityId = 0,
                Name = "[Selecione um Registro ...]"
            });

            return cities = cities.OrderBy(c => c.Name).ToList();

        }

        public static List<City> GetCities(int stateId)
        {
            var cities = db.Cities.Where(c => c.StateId == stateId).ToList();
            cities.Add(new City
            {
                CityId = 0,
                Name = "[Selecione um Registro ...]",
            });

            return cities.OrderBy(d => d.Name).ToList();
        }

        public static List<Company> GetCompanies()
        {
            var companies = db.Companies.ToList();

            companies.Add(new Company
            {
                CompanyId = 0,
                Name = "[Selecione um Registro ...]"
            });

            return companies = companies.OrderBy(d => d.CompanyId).ToList();

        }

        public static List<Company> GetCompanies(int companyId)
        {
            var empresas = db.Companies.Where(c => c.CompanyId == companyId).ToList();
            empresas.Add(new Company
            {
                CompanyId = 0,
                Name = "[Selecione um Registro ...]",
            });

            return empresas.OrderBy(d => d.CompanyId).ToList();
        }

        public static List<Country> GetCountries()
        {
            var countries = db.Countries.ToList();

            countries.Add(new Country
            {
                CountryId = 0,
                Name = "[Selecione um Registro ...]"
            });

            return countries = countries.OrderBy(d => d.Name).ToList();

        }

        public static List<Country> GetCountries(int countryId)
        {
            var countries = db.Countries.Where(p => p.CountryId == countryId).ToList();

            countries.Add(new Country
            {
                CountryId = 0,
                Name = "[Selecione um Registro ...]"
            });

            return countries = countries.OrderBy(d => d.Name).ToList();
        }
    }
}
