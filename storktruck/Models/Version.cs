﻿namespace storktruck.Models
{
    using Newtonsoft.Json;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Version
    {
        [Key]
        [Display(Name = "´Cód.Versão")]
        public int VersionId { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        [Display(Name = "Modelo")]
        public int ModelId { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        [MaxLength(50, ErrorMessage = "O campo {0} pode ter no máximo {1} caracteres.")]
        [Display(Name = "Modelo")]
        [Index("Version_VersionName_Index", IsUnique = true)]
        public string VersionName { get; set; }

        [JsonIgnore]
        public virtual Model Model { get; set; }
    }
}