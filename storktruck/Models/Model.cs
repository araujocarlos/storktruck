﻿namespace storktruck.Models
{
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Model
    {
        [Key]
        [Display(Name = "´Cód.Modelo")]
        public int ModelId { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        [Display(Name = "Marca")]
        public int BrandId { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        [MaxLength(50, ErrorMessage = "O campo {0} pode ter no máximo {1} caracteres.")]
        [Display(Name = "Modelo")]
        [Index("Model_Name_Index", IsUnique = true)]
        public string ModelName { get; set; }

        [JsonIgnore]
        public virtual Brand Brand { get; set; }

        [JsonIgnore]
        public virtual ICollection<Version> Versions { get; set; }

    }
}