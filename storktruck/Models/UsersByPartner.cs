﻿namespace storktruck.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    public class UsersByPartner
    {
        public string Id { get; set; }

        public int PartnerId { get; set; }
    }
}