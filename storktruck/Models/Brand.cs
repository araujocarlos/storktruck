﻿namespace storktruck.Models
{
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Brand
    {
        [Key]
        public int BrandId { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        [MaxLength(50, ErrorMessage = "O campo {0} pode ter no máximo {1} caracteres.")]
        [Display(Name = "Marca")]
        [Index("Brand_Name_Index", IsUnique = true)]
        public string BrandName { get; set; }

        [JsonIgnore]
        public virtual ICollection<Model> Models { get; set; }
    }
}