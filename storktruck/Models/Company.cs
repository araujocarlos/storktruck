﻿namespace storktruck.Models
{
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Web;
    using storktruck.Classes;

    public class Company
    {
        [Key]
        public int CompanyId { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório..")]
        [MaxLength(50, ErrorMessage = "O campo {0} pode ter no máximo {1} caracteres.")]
        [Display(Name = "Nome")]
        [Index("Company_Name_Index", IsUnique = true)]
        public string Name { get; set; }

        //[Required(ErrorMessage = "O campo {0} é obrigatório..")]
        [MaxLength(50, ErrorMessage = "O campo {0} pode ter no máximo {1} caracteres.")]
        [Display(Name = "Fantasia")]        
        public string FantasyName { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        [MaxLength(50, ErrorMessage = "O campo {0} pode ter no máximo {1} caracteres.")]
        [DisplayFormat(DataFormatString = "{0:00\\.000\\.000\\/0000\\-00}", ApplyFormatInEditMode = true)]
        [Index("CNPJ_Name_Index", IsUnique = true)]
        [Cnpj(ErrorMessage = "CNPJ Inválido")]
        [Display(Name = "CNPJ")]
        public string SocialNumberJ { get; set; }
        
        //[Required(ErrorMessage = "O campo {0} é obrigatório.")]
        [MaxLength(50, ErrorMessage = "O campo {0} pode ter no máximo {1} caracteres.")]
        //[DisplayFormat(DataFormatString = "{0:00\\.000\\.000\\/0000\\-00}", ApplyFormatInEditMode = true)]
        //[Index("CNPJ_Name_Index", IsUnique = true)]
        [Display(Name = "Insc.Estadual")]
        public string StateSocialNumber { get; set; }

        //[Required(ErrorMessage = "O campo {0} é obrigatório.")]
        [MaxLength(50, ErrorMessage = "O campo {0} pode ter no máximo {1} caracteres.")]
        //[DisplayFormat(DataFormatString = "{0:00\\.000\\.000\\/0000\\-00}", ApplyFormatInEditMode = true)]
        //[Index("CNPJ_Name_Index", IsUnique = true)]
        [Display(Name = "Insc.Municipal")]
        public string CountySocialNumber { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório..")]
        [MaxLength(20, ErrorMessage = "O campo {0} pode ter no máximo {1} caracteres.")]
        [Display(Name = "Fone")]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório..")]
        [MaxLength(100, ErrorMessage = "O campo {0} pode ter no máximo {1} caracteres.")]
        [Display(Name = "Endereço")]
        public string Address { get; set; }

        [MaxLength(100, ErrorMessage = "O campo {0} pode ter no máximo {1} caracteres.")]
        [Display(Name = "Complemento")]
        public string Complement { get; set; }

        [MaxLength(100, ErrorMessage = "O campo {0} pode ter no máximo {1} caracteres.")]
        [Display(Name = "Bairro")]
        public string Neighborhood { get; set; }

        [MaxLength(10, ErrorMessage = "O campo {0} pode ter no máximo {1} caracteres.")]
        [Display(Name = "CEP")]
        [DisplayFormat(DataFormatString = "{0:00\\.000\\-000}", ApplyFormatInEditMode = true)]
        public string ZipCode { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        [Range(1, double.MaxValue, ErrorMessage = "Você precisa selecionar o(a) {0}")]
        [Display(Name = "País")]
        public int CountryId { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        [Range(1, double.MaxValue, ErrorMessage = "Você precisa selecionar o(a) {0}")]
        [Display(Name = "Estado")]
        public int StateId { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        [Range(1, double.MaxValue, ErrorMessage = "Você precisa selecionar o(a) {0}")]
        [Display(Name = "Cidade")]
        public int CityId { get; set; }

        [DataType(DataType.ImageUrl)]
        [Display(Name = "Logo")]
        public string Logo { get; set; }

        [NotMapped]
        [Display(Name = "Logo")]
        public HttpPostedFileBase LogoFile { get; set; }

        [JsonIgnore]
        public virtual Country Country { get; set; }

        [JsonIgnore]
        public virtual State State { get; set; }

        [JsonIgnore]
        public virtual City City { get; set; }

        //[JsonIgnore]
        //public virtual ICollection<AccountChart> AccountCharts { get; set; }

        //[JsonIgnore]
        //public virtual ICollection<Customer> Customers { get; set; }

        //[JsonIgnore]
        //public virtual ICollection<Supplier> Suppliers { get; set; }

        //[JsonIgnore]
        //public virtual ICollection<User> Users { get; set; }

        //[JsonIgnore]
        //public virtual ICollection<Warehouse> Warehouses { get; set; }

        //[JsonIgnore]
        //public virtual ICollection<Seller> Sellers { get; set; }

        //[JsonIgnore]
        //public virtual ICollection<Manufacturer> Manufacturers { get; set; }

        //[JsonIgnore]
        //public virtual ICollection<Product> Products { get; set; }

        //[JsonIgnore]
        //public virtual ICollection<Purchase> Purchases { get; set; }

        //[JsonIgnore]
        //public virtual ICollection<Cash> Cashes { get; set; }

        //[JsonIgnore]
        //public virtual ICollection<PaymentType> PaymentTypes { get; set; }
    }
}