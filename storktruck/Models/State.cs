﻿namespace storktruck.Models
{
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    public class State
    {
        [Key]
        public int StateId { get; set; }

        [Display(Name = "Nome do País")]
        public int CountryId { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório..")]
        [MaxLength(50, ErrorMessage = "O campo {0} pode ter no máximo {1} caracteres.")]
        [Display(Name = "Nome do Estado")]
        [Index("State_Name_Index", IsUnique = true)]
        public string Name { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório..")]
        [MaxLength(2, ErrorMessage = "O campo {0} pode ter no máximo {1} caracteres.")]
        [Display(Name = "Sigla do Estado")]
        public string Initials { get; set; }

        [JsonIgnore]
        public virtual Country Country { get; set; }

        [JsonIgnore]
        public virtual ICollection<City> Cities { get; set; }

        [JsonIgnore]
        public virtual ICollection<Company> Companies { get; set; }

        [JsonIgnore]
        public virtual ICollection<User> Users { get; set; }

        //[JsonIgnore]
        //public virtual ICollection<Customer> Customers { get; set; }

        //[JsonIgnore]
        //public virtual ICollection<Supplier> Suppliers { get; set; }

        //[JsonIgnore]
        //public virtual ICollection<Seller> Sellers { get; set; }

        //[JsonIgnore]
        //public virtual ICollection<Manufacturer> Manufacturers { get; set; }

    }
}