namespace storktruck.Models
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Conventions;
    public class DataContext : DbContext
    {
        public DataContext() : base("DefaultConnection")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        public System.Data.Entity.DbSet<storktruck.Models.User> Users { get; set; }
        public System.Data.Entity.DbSet<storktruck.Models.Country> Countries { get; set; }

        public System.Data.Entity.DbSet<storktruck.Models.State> States { get; set; }

        public System.Data.Entity.DbSet<storktruck.Models.City> Cities { get; set; }

        public System.Data.Entity.DbSet<storktruck.Models.Company> Companies { get; set; }

        public System.Data.Entity.DbSet<storktruck.Models.Partner> Partners { get; set; }

        public System.Data.Entity.DbSet<storktruck.Models.UsersByPartner> UsersByPartners { get; set; }

        public System.Data.Entity.DbSet<storktruck.Models.Brand> Brands { get; set; }

        public System.Data.Entity.DbSet<storktruck.Models.Model> Models { get; set; }

        public System.Data.Entity.DbSet<storktruck.Models.Version> Versions { get; set; }
    }
}