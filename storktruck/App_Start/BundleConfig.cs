﻿namespace storktruck
{
    using System.Web.Optimization;

    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery-ui.min.js",
                        "~/Scripts/jquery.dataTables.min.js",
                        "~/Scripts/dataTables.select.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquerycarousel").Include(
                        "~/Scripts/jcarousellite_1.0.1.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/CustomValidacoes.js"));

            bundles.Add(new ScriptBundle("~/bundles/Mascaras").Include(
                    "~/Scripts/Mascaras/jquery.mask.js",
                    "~/Scripts/Mascaras/Mascaras.js"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.min.js",
                      "~/Scripts/Chart.js",
                      "~/Scripts/moment.js",
                      "~/Scripts/fileupload.js",
                      "~/Scripts/bootstrap-treeview.js",
                      "~/Scripts/bootstrap-datetimepicker.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/bootstrap-datetimepicker.css",
                      "~/Content/site.css",
                      "~/Content/jquery.dataTables.min.css",
                      "~/Content/select.dataTables.min.css",
                      "~/Content/bootstrap-treeview.css",                      
                      "~/Content/jquery-ui.min.css",
                      "~/Content/font-awesome.css"));
        }
    }
}
