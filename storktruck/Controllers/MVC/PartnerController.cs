﻿namespace storktruck.Controllers.MVC
{
    using System.Data;
    using System.Data.Entity;
    using System.Linq;
    using System.Net;
    using System.Web.Mvc;
    using storktruck.Classes;
    using storktruck.Models;

    [Authorize(Roles = "Admin")]
    public class PartnerController : Controller
    {
        private DataContext db = new DataContext();

        // GET: storktruck
        public ActionResult Index()
        {
            var user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            var partners = db.Partners.Include(p => p.City).Include(p => p.Country).Include(p => p.State);
            return View(partners.ToList());
        }

        // GET: storktruck/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Partner partner = db.Partners.Find(id);
            if (partner == null)
            {
                return HttpNotFound();
            }
            return View(partner);
        }

        // GET: storktruck/Create
        public ActionResult Create()
        {

            ViewBag.CityId = new SelectList(CombosHelper.GetCities(), "CityId", "Name");
            //ViewBag.CompanyId = new SelectList(CombosHelper.GetCompanies(), "CompanyId", "Name");
            ViewBag.CountryId = new SelectList(CombosHelper.GetCountries(), "CountryId", "Name");
            ViewBag.StateId = new SelectList(CombosHelper.GetStates(), "StateId", "Name");
            return View();
        }

        // POST: storktruck/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Partner partner)
        {
            if (ModelState.IsValid)
            {
                db.Partners.Add(partner);
                var response = DBHelper.SaveChanges(db);
                if (response.Succeeded)
                {
                    if (partner.LogoFile != null)
                    {
                        var folder = "~/Content/Partners";
                        var file = string.Format("{0}.jpg", partner.PartnerId);
                        var response1 = FileHelper.UploadPhoto(partner.LogoFile, folder, file);
                        if (response1)
                        {
                            var pic = string.Format("{0}/{1}", folder, file);
                            partner.Logo = pic;
                            db.Entry(partner).State = EntityState.Modified;
                            var response2 = DBHelper.SaveChanges(db);
                            if (!response2.Succeeded)
                            {
                                ModelState.AddModelError(string.Empty, response.Message);
                            }
                        }
                    }
                    return RedirectToAction("Index");
                }

                ModelState.AddModelError(string.Empty, response.Message);
            }
            ViewBag.CityId = new SelectList(CombosHelper.GetCities(partner.StateId), "CityId", "Name", partner.CityId);
            //ViewBag.CompanyId = new SelectList(CombosHelper.GetCompanies(partner.CompanyId), "CompanyId", "Name", partner.CompanyId);
            ViewBag.CountryId = new SelectList(CombosHelper.GetCountries(partner.CountryId), "CountryId", "Name", partner.CountryId);
            ViewBag.StateId = new SelectList(CombosHelper.GetStates(partner.CountryId), "StateId", "Name", partner.StateId);
            return View(partner);
        }

        // GET: storktruck/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Partner partner = db.Partners.Find(id);
            if (partner == null)
            {
                return HttpNotFound();
            }
            ViewBag.CityId = new SelectList(CombosHelper.GetCities(partner.StateId), "CityId", "Name", partner.CityId);
            //ViewBag.CompanyId = new SelectList(CombosHelper.GetCompanies(partner.CompanyId), "CompanyId", "Name", partner.CompanyId);
            ViewBag.StateId = new SelectList(CombosHelper.GetStates(partner.CountryId), "StateId", "Name", partner.StateId);
            ViewBag.CountryId = new SelectList(CombosHelper.GetCountries(), "CountryId", "Name", partner.CountryId);
            return View(partner);
        }

        // POST: storktruck/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Partner partner)
        {
            if (ModelState.IsValid)
            {
                if (partner.LogoFile != null)
                {
                    var pic = string.Empty;
                    var folder = "~/Content/Partners";
                    var file = string.Format("{0}.jpg", partner.PartnerId);
                    var response1 = FileHelper.UploadPhoto(partner.LogoFile, folder, file);
                    if (response1)
                    {
                        pic = string.Format("{0}/{1}", folder, file);
                        partner.Logo = pic;
                    }
                }

                db.Entry(partner).State = EntityState.Modified;
                var response = DBHelper.SaveChanges(db);
                if (response.Succeeded)
                {
                    return RedirectToAction("Index");
                }

                ModelState.AddModelError(string.Empty, response.Message);
            }
            ViewBag.CityId = new SelectList(CombosHelper.GetCities(partner.StateId), "CityId", "Name", partner.CityId);
            //ViewBag.CompanyId = new SelectList(CombosHelper.GetCompanies(partner.CompanyId), "CompanyId", "Name", partner.CompanyId);
            ViewBag.StateId = new SelectList(CombosHelper.GetStates(partner.CountryId), "StateId", "Name", partner.StateId);
            ViewBag.CountryId = new SelectList(CombosHelper.GetCountries(), "CountryId", "Name", partner.CountryId);
            return View(partner);
        }

        // GET: storktruck/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Partner partner = db.Partners.Find(id);
            if (partner == null)
            {
                return HttpNotFound();
            }
            return View(partner);
        }

        // POST: storktruck/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Partner partner = db.Partners.Find(id);
            db.Partners.Remove(partner);
            var response = DBHelper.SaveChanges(db);
            if (response.Succeeded)
            {
                return RedirectToAction("Index");
            }
            ModelState.AddModelError(string.Empty, response.Message);
            return View(partner);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
