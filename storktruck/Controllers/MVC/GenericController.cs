﻿namespace storktruck.MVC.Controllers
{
    using System.Linq;
    using System.Web.Mvc;
    using storktruck.Models;

    public class GenericController : Controller
    {
        private DataContext db = new DataContext();

        public JsonResult GetCities(int stateId)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var cities = db.Cities.Where(c => c.StateId == stateId).OrderBy(c => c.Name);
            return Json(cities);
        }

        public JsonResult GetStates(int countryId)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var states = db.States.Where(c => c.CountryId == countryId).OrderBy(c => c.Name);
            return Json(states);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}