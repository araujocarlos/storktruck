﻿namespace storktruck.Controllers.MVC
{
    using System.Data.Entity;
    using System.Linq;
    using System.Net;
    using System.Web.Mvc;
    using storktruck.Classes;
    using storktruck.Models;

    [Authorize(Roles = "Admin")]
    public class CompanyController : Controller
    {
        private DataContext db = new DataContext();

        // GET: Company
        public ActionResult Index()
        {
            //var user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            var companies = db.Companies.Include(c => c.City).Include(c => c.Country).Include(c => c.State);
            return View(companies.ToList());
        }

        // GET: Company/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Company company = db.Companies.Find(id);
            if (company == null)
            {
                return HttpNotFound();
            }
            return View(company);
        }

        // GET: Company/Create
        public ActionResult Create()
        {
            ViewBag.CityId = new SelectList(CombosHelper.GetCities(), "CityId", "Name");
            ViewBag.StateId = new SelectList(CombosHelper.GetStates(), "StateId", "Name");
            ViewBag.CountryId = new SelectList(CombosHelper.GetCountries(), "CountryId", "Name");
            return View();
        }

        // POST: Company/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Company company)
        {
            if (ModelState.IsValid)
            {
                db.Companies.Add(company);
                var response = DBHelper.SaveChanges(db);
                if (response.Succeeded)
                {
                    if (company.LogoFile != null)
                    {
                        var folder = "~/Content/Logos";
                        var file = string.Format("{0}.jpg", company.CompanyId);
                        var response1 = FileHelper.UploadPhoto(company.LogoFile, folder, file);
                        if (response1)
                        {
                            var pic = string.Format("{0}/{1}", folder, file);
                            company.Logo = pic;
                            db.Entry(company).State = EntityState.Modified;
                            var response2 = DBHelper.SaveChanges(db);
                            if (!response2.Succeeded)
                            {
                                ModelState.AddModelError(string.Empty, response.Message);
                            }
                        }
                    }
                    return RedirectToAction("Index");
                }

                ModelState.AddModelError(string.Empty, response.Message);
            }

            ViewBag.CityId = new SelectList(CombosHelper.GetCities(company.StateId), "CityId", "Name", company.CityId);
            ViewBag.StateId = new SelectList(CombosHelper.GetStates(company.CountryId), "StateId", "Name", company.StateId);
            ViewBag.CountryId = new SelectList(CombosHelper.GetCountries(), "CountryId", "Name", company.CountryId);

            return View(company);
        }

        // GET: Company/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Company company = db.Companies.Find(id);
            if (company == null)
            {
                return HttpNotFound();
            }
            ViewBag.CityId = new SelectList(CombosHelper.GetCities(company.StateId), "CityId", "Name", company.CityId);
            ViewBag.StateId = new SelectList(CombosHelper.GetStates(company.CountryId), "StateId", "Name", company.StateId);
            ViewBag.CountryId = new SelectList(CombosHelper.GetCountries(), "CountryId", "Name", company.CountryId);
            return View(company);
        }

        // POST: Company/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Company company)
        {
            if (ModelState.IsValid)
            {
                if (company.LogoFile != null)
                {
                    var pic = string.Empty;
                    var folder = "~/Content/Logos";
                    var file = string.Format("{0}.jpg", company.CompanyId);
                    var response1 = FileHelper.UploadPhoto(company.LogoFile, folder, file);
                    if (response1)
                    {
                        pic = string.Format("{0}/{1}", folder, file);
                        company.Logo = pic;
                    }
                }

                db.Entry(company).State = EntityState.Modified;
                var response = DBHelper.SaveChanges(db);
                if (response.Succeeded)
                {
                    return RedirectToAction("Index");
                }

                ModelState.AddModelError(string.Empty, response.Message);
            }

            ViewBag.CityId = new SelectList(CombosHelper.GetCities(company.StateId), "CityId", "Name", company.CityId);
            ViewBag.StateId = new SelectList(CombosHelper.GetStates(company.CountryId), "StateId", "Name", company.StateId);
            ViewBag.CountryId = new SelectList(CombosHelper.GetCountries(), "CountryId", "Name", company.CountryId);
            return View(company);
        }

        // GET: Company/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Company company = db.Companies.Find(id);
            if (company == null)
            {
                return HttpNotFound();
            }
            return View(company);
        }

        // POST: Company/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Company company = db.Companies.Find(id);
            db.Companies.Remove(company);
            var response = DBHelper.SaveChanges(db);
            if (response.Succeeded)
            {
                return RedirectToAction("Index");
            }
            ModelState.AddModelError(string.Empty, response.Message);
            return View(company);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
