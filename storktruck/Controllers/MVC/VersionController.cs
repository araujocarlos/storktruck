﻿namespace storktruck.Controllers.MVC
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Entity;
    using System.Linq;
    using System.Net;
    using System.Web;
    using System.Web.Mvc;
    using storktruck.Classes;
    using storktruck.Models;

    [Authorize(Roles = "Admin")]
    public class VersionController : Controller
    {
        private DataContext db = new DataContext();

        // GET: Version
        public ActionResult Index()
        {
            var user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            var versions = db.Versions.Include(v => v.Model);
            return View(versions.ToList());
        }

        // GET: Version/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Models.Version version = db.Versions.Find(id);
            if (version == null)
            {
                return HttpNotFound();
            }
            return View(version);
        }

        // GET: Version/Create
        public ActionResult Create()
        {
            ViewBag.ModelId = new SelectList(CombosHelper.GetModels(), "ModelId", "ModelName");
            return View();
        }

        // POST: Version/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Models.Version version)
        {
            if (ModelState.IsValid)
            {
                db.Versions.Add(version);
                var response = DBHelper.SaveChanges(db);
                if (response.Succeeded)
                {
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError(string.Empty, response.Message);
            }

            ViewBag.ModelId = new SelectList(CombosHelper.GetModels(version.ModelId), "ModelId", "ModelName", version.ModelId);
            return View(version);
        }

        // GET: Version/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Models.Version version = db.Versions.Find(id);
            if (version == null)
            {
                return HttpNotFound();
            }
            ViewBag.ModelId = new SelectList(CombosHelper.GetModels(version.ModelId), "ModelId", "ModelName", version.ModelId);
            return View(version);
        }

        // POST: Version/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Models.Version version)
        {
            if (ModelState.IsValid)
            {
                db.Entry(version).State = EntityState.Modified;
                var response = DBHelper.SaveChanges(db);
                if (response.Succeeded)
                {
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError(string.Empty, response.Message);

            }
            ViewBag.ModelId = new SelectList(CombosHelper.GetModels(version.ModelId), "ModelId", "ModelName", version.ModelId);
            return View(version);
        }

        // GET: Version/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Models.Version version = db.Versions.Find(id);
            if (version == null)
            {
                return HttpNotFound();
            }
            return View(version);
        }

        // POST: Version/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Models.Version version = db.Versions.Find(id);
            db.Versions.Remove(version);
            var response = DBHelper.SaveChanges(db);
            if (response.Succeeded)
            {
                return RedirectToAction("Index");
            }
            ModelState.AddModelError(string.Empty, response.Message);
            return View(version);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
