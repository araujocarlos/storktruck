﻿namespace storktruck.Controllers.MVC
{
    using System.Data;
    using System.Data.Entity;
    using System.Linq;
    using System.Net;
    using System.Web.Mvc;
    using Models;
    using storktruck.Classes;

    [Authorize(Roles = "Admin")]
    public class UserController : Controller
    {
        private DataContext db = new DataContext();

        // GET: User
        public ActionResult Index()
        {
            //var user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            //var users = db.Users.Include(u => u.City).Include(u => u.Company).Include(u => u.Country).Include(u => u.State);
            var users = db.Users.Include(u => u.City).Include(u => u.Country).Include(u => u.State).Include(u => u.Partner);
            return View(users.ToList());
        }

        // GET: User/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: User/Create
        public ActionResult Create()
        {
            ViewBag.CityId = new SelectList(CombosHelper.GetCities(), "CityId", "Name");
            ViewBag.storktruckId = new SelectList(CombosHelper.GetPartners(), "storktruckId", "Name");
            ViewBag.CountryId = new SelectList(CombosHelper.GetCountries(), "CountryId", "Name");
            ViewBag.StateId = new SelectList(CombosHelper.GetStates(), "StateId", "Name");
            return View();
        }

        // POST: User/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(User user)
        {
            if (ModelState.IsValid)
            {
                db.Users.Add(user);
                var response = DBHelper.SaveChanges(db);
                if (response.Succeeded)
                {
                    UsersHelper.CreateUserASP(user.UserName, "User");

                    if (user.PhotoFile != null)
                    {
                        var folder = "~/Content/Users";
                        var file = string.Format("{0}.jpg", user.UserId);
                        var response1 = FileHelper.UploadPhoto(user.PhotoFile, folder, file);
                        if (response1)
                        {
                            var pic = string.Format("{0}/{1}", folder, file);
                            user.Photo = pic;
                            db.Entry(user).State = EntityState.Modified;

                            var response2 = DBHelper.SaveChanges(db);
                            if (!response2.Succeeded)
                            {
                                ModelState.AddModelError(string.Empty, response.Message);
                            }
                        }
                    }
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError(string.Empty, response.Message);
            }
            ViewBag.CityId = new SelectList(CombosHelper.GetCities(user.StateId), "CityId", "Name", user.CityId);
            ViewBag.storktruckId = new SelectList(CombosHelper.GetPartners(user.PartnerId), "storktruckId", "Name", user.PartnerId);
            ViewBag.CountryId = new SelectList(CombosHelper.GetCountries(user.CountryId), "CountryId", "Name", user.CountryId);
            ViewBag.StateId = new SelectList(CombosHelper.GetStates(user.CountryId), "StateId", "Name", user.StateId);
            return View(user);
        }

        // GET: User/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            ViewBag.CityId = new SelectList(CombosHelper.GetCities(user.StateId), "CityId", "Name", user.CityId);
            ViewBag.storktruckId = new SelectList(CombosHelper.GetPartners(user.PartnerId), "storktruckId", "Name", user.PartnerId);
            ViewBag.CountryId = new SelectList(CombosHelper.GetCountries(user.CountryId), "CountryId", "Name", user.CountryId);
            ViewBag.StateId = new SelectList(CombosHelper.GetStates(user.CountryId), "StateId", "Name", user.StateId);
            return View(user);
        }

        // POST: User/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(User user)
        {
            if (ModelState.IsValid)
            {
                if (user.PhotoFile != null)
                {
                    var file = string.Format("{0}.jpg", user.UserId);
                    var folder = "~/Content/Users";
                    var response1 = FileHelper.UploadPhoto(user.PhotoFile, folder, file);
                    user.Photo = string.Format("{0}/{1}", folder, file);
                }

                // Como estamos editando o usuario e possivel que o email tenha sido alterado. Para saber isto e
                // efetuarmos as atualizações devemos recuperar o usuario anterior. Para isto definir um novo 
                // contexto de dados e recuperar o usuario. Fazer as comparacoes. Se forem diferente atualizar o 
                // o usuario e seu email e fechar a conexão temporária criada para acessar o BD.

                var db2 = new DataContext();
                var currentUser = db2.Users.Find(user.UserId);
                if (currentUser.UserName != user.UserName)
                {
                    UsersHelper.UpdateUserName(currentUser.UserName, user.UserName);
                }
                db2.Dispose();

                db.Entry(user).State = EntityState.Modified;
                var response = DBHelper.SaveChanges(db);
                if (response.Succeeded)
                {
                    return RedirectToAction("Index");
                }

                ModelState.AddModelError(string.Empty, response.Message);

            }
            ViewBag.CityId = new SelectList(CombosHelper.GetCities(user.StateId), "CityId", "Name", user.CityId);
            ViewBag.storktruckId = new SelectList(CombosHelper.GetPartners(user.PartnerId), "storktruckId", "Name", user.PartnerId);
            ViewBag.CountryId = new SelectList(CombosHelper.GetCountries(user.CountryId), "CountryId", "Name", user.CountryId);
            ViewBag.StateId = new SelectList(CombosHelper.GetStates(user.CountryId), "StateId", "Name", user.StateId);
            return View(user);
        }

        // GET: User/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: User/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            var response = DBHelper.SaveChanges(db);
            if (response.Succeeded)
            {
                UsersHelper.DeleteUser(user.UserName, "User");
                return RedirectToAction("Index");
            }
            ModelState.AddModelError(string.Empty, response.Message);
            return View(user);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
